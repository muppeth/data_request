OC.L10N.register(
    "data_request",
    {
    "sent!" : "απεστάλη!",
    "Hello %s," : "Γεια σου %s,",
    "Account deletion request" : "Αίτηση διαγραφής λογαριασμού",
    "No administrator has set an email address" : "Κανένας διαχειριστής δεν όρισε μια διεύθυνση ηλεκτρονικού ταχυδρομείου",
    "Data Request" : "Αίτημα δεδομένων",
    "Request your data from the admins" : "Αιτηθείτε τα δεδομένα από τους διαχειριστές",
    "Account" : "Λογαριασμός",
    "Request account deletion" : "Αίτηση διαγραφής λογαριασμού",
    "Enable your users to request an export or deletion of their data. According options are added to the personal settings section. Administrations will be notified by email about the request." : "Ενεργοποίηση των αιτημάτων χρηστών για εξαγωγή ή διαγραφή των δεδομένων τους. Σύμφωνα με τις επιλογές που έχουν προστεθεί στο τμήμα των προσωπικών ρυθμίσεων.Οι διαχειρηστές θα ειδοποιούνται με email για το αίτημα."
},
"nplurals=2; plural=(n != 1);");
