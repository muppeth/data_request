OC.L10N.register(
    "data_request",
    {
    "sent!" : "bidalita!",
    "No administrator could have been contacted." : "Ezin izan da administratzailerik kontaktatu.",
    "Hello %s," : "Kaixo %s,",
    "Data Request" : "Datu eskaera",
    "Request your data from the admins" : "Eskatu zure datuak administratzaileei",
    "Account" : "Kontua",
    "Request data export" : "Eskatu datuen esportazioa",
    "Request account deletion" : "Eskatua kontuaren ezabaketa"
},
"nplurals=2; plural=(n != 1);");
